package tdd.training.mra;

import static org.junit.Assert.*;
import java.util.List;
import java.util.ArrayList;
import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void planetContainsObstacleAttest() throws MarsRoverException {
		List<String> ob = new ArrayList<>();
		ob.add("(5,5)");
		ob.add("(1,2)");
		
		MarsRover mr = new MarsRover(10,12,ob);
		assertTrue(mr.planetContainsObstacleAt(1, 2));
		assertFalse(mr.planetContainsObstacleAt(1, 3));
	}
	
	@Test
	public void executeCommandtest() throws MarsRoverException {
		List<String> ob = new ArrayList<>();
		
		MarsRover mr = new MarsRover(10,12,ob);
		assertEquals(mr.executeCommand(""),"(0,0,N)");
		assertEquals(mr.executeCommand("r"),"(0,0,E)");
		assertEquals(mr.executeCommand("r"),"(0,0,S)");
		assertEquals(mr.executeCommand("l"),"(0,0,E)");
		mr.executeCommand("l");
		assertEquals(mr.executeCommand("l"),"(0,0,W)");
		assertEquals(mr.executeCommand("l"),"(0,0,S)");
	}
	
	@Test
	public void executeCommandFronttest() throws MarsRoverException {
		List<String> ob = new ArrayList<>();
		
		MarsRover mr = new MarsRover(10,12,ob);
		assertEquals(mr.executeCommand("f"),"(0,1,N)");
		mr.executeCommand("r");
		assertEquals(mr.executeCommand("f"),"(1,1,E)");
		mr.executeCommand("l");
		assertEquals(mr.executeCommand("f"),"(1,2,N)");
		mr.executeCommand("l");
		assertEquals(mr.executeCommand("f"),"(0,2,W)");
		mr.executeCommand("l");
		assertEquals(mr.executeCommand("f"),"(0,1,S)");
		
	}
	
	@Test
	public void executeCommandBacktest() throws MarsRoverException {
		List<String> ob = new ArrayList<>();
		
		MarsRover mr = new MarsRover(10,12,ob);
		assertEquals(mr.executeCommand("b"),"(0,11,N)");
		mr.executeCommand("r");
		assertEquals(mr.executeCommand("b"),"(9,11,E)");
		mr.executeCommand("l");
		assertEquals(mr.executeCommand("b"),"(9,10,N)");
		mr.executeCommand("l");
		assertEquals(mr.executeCommand("b"),"(0,10,W)");
		mr.executeCommand("l");
		assertEquals(mr.executeCommand("b"),"(0,11,S)");
	}
	
	@Test
	public void executeCommandCombinedtest() throws MarsRoverException {
		List<String> ob = new ArrayList<>();
		
		MarsRover mr = new MarsRover(10,12,ob);
		assertEquals(mr.executeCommand("br"),"(0,11,E)");
		assertEquals(mr.executeCommand("lf"),"(0,0,N)");
		assertEquals(mr.executeCommand("ffllbbrr"),"(0,4,N)");
	}
	
	@Test
	public void executeCommandWithObstaclestest() throws MarsRoverException {
		List<String> ob = new ArrayList<>();
		ob.add("(0,2)");
		ob.add("(0,1)");
		ob.add("(1,1)");
		ob.add("(3,0)");
		ob.add("(0,11)");
		MarsRover mr = new MarsRover(10,12,ob);
		assertEquals(mr.executeCommand("b"),"(0,0,N)(0,11)");
		assertEquals(mr.executeCommand("fff"),"(0,0,N)(0,1)");
		assertEquals(mr.executeCommand("rflf"),"(1,0,N)(1,1)");
		assertEquals(mr.executeCommand("rfflfflff"),"(1,2,W)(3,0)(0,2)");
	}
	
	@Test
	public void executeCommandTourstest() throws MarsRoverException {
		List<String> ob = new ArrayList<>();
		ob.add("(2,2)");
		ob.add("(0,5)");
		ob.add("(5,0)");
		MarsRover mr = new MarsRover(6,6,ob);
		assertEquals(mr.executeCommand("ffrfffrbbblllfrfrbbl"),"(0,0,N)(2,2)(0,5)(5,0)");
	}
	
}
