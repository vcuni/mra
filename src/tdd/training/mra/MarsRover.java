package tdd.training.mra;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MarsRover {

	boolean[][] planet;
	int planetX;
	int planetY;
	int roverX;
	int roverY;
	List<String> direction;
	int roverdirection;
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		planet = new boolean [planetX][planetY];
		direction= Arrays.asList("N","E","S","W");
		this.planetX = planetX;
		this.planetY = planetY;
		int x = 0;
		int y = 0;
		for(int i=0;i<planetX;i++)
			for(int j=0;j<planetY;j++)planet[i][j]=false;
		
		for(String str:planetObstacles) {
			try {
				x = Integer.parseInt(str.substring(str.indexOf("(")+1, str.indexOf(",")));
				y = Integer.parseInt(str.substring(str.indexOf(",")+1, str.indexOf(")")));
				planet[x][y]=true;
			} catch(Exception e) {
				throw new MarsRoverException("invalid list of obstacles");
			}
		}
		roverX = 0;
		roverY = 0;
		roverdirection=0;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		if(x>=planetX || y>=planetY || x<0 || y<0) throw new MarsRoverException("location out of border");
		return planet[x][y];
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		char[] commands = commandString.toCharArray();
		StringBuilder obs = new StringBuilder();
		List<String> obstacles = new ArrayList<>();
		if(commandString.equals("")) return "("+roverX+","+roverY+","+direction.get(roverdirection)+")";
		for(char singleCommand :commands) {
			if(singleCommand=='r') {
				roverdirection = (roverdirection+1)%4;
			}
			else if(singleCommand=='l') {
				if(roverdirection == 0) roverdirection = direction.size()-1;
				else roverdirection--;
			}
			else if(singleCommand=='f') {
				move(1, obstacles);
			}
			else if(singleCommand=='b') {
				move(-1, obstacles);
			}
			else throw new MarsRoverException("command undefined");
		}
		for(String s: obstacles) obs.append(s);
		return "("+roverX+","+roverY+","+direction.get(roverdirection)+")"+obs;
	}
	
	
	private void move(int toward, List<String> obstacles) {
		if(roverdirection==0) {
			if(isObstacle(roverX, (roverY+toward)%planetY, obstacles)) return;
			roverY = (roverY+toward)%planetY;
			if(roverY<0) roverY = planetY-1;
		}
		if(roverdirection==1) {
			if(isObstacle((roverX+toward)%planetX, roverY, obstacles)) return;
			roverX = (roverX+toward)%planetX;
			if(roverX<0) roverX = planetX-1;
		}
		if(roverdirection==2) {
			if(isObstacle(roverX, (roverY-toward)%planetY, obstacles)) return;
			roverY = (roverY-toward)%planetY;
			if(roverY<0) roverY = planetY-1;
		}
		if(roverdirection==3) {
			if(isObstacle((roverX-toward)%planetX, roverY, obstacles)) return;
			roverX = (roverX-toward)%planetX;
			if(roverX<0) roverX = planetX-1;
		}
	}
	
	
	private boolean isObstacle(int x, int y, List<String> obstacles) {
		if(x<0) x = planetX-1;
		if(y<0) y = planetY-1;
		if(planet[x][y]) {
			String obstacle = String.format("(%d,%d)", x, y);
			if(obstacles.contains(obstacle)) return true;
			obstacles.add(obstacle);
			return true;
		}
		return false;
	}

}
